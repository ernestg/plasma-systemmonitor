# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-systemmonitor package.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-systemmonitor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-22 02:08+0000\n"
"PO-Revision-Date: 2023-06-11 08:26+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: src/faces/processtable/contents/ui/Config.qml:24
#, kde-format
msgctxt "@option:check"
msgid "Confirm ending processes."
msgstr "Confirmar a finalización dos procesos."

#: src/faces/processtable/contents/ui/Config.qml:29
#, kde-format
msgctxt "@label:listbox"
msgid "By default show:"
msgstr "Amosar de maneira predeterminada:"

#: src/faces/processtable/contents/ui/Config.qml:38
#, kde-format
msgctxt "@item:inlistbox"
msgid "Own Processes"
msgstr "Procesos propios"

#: src/faces/processtable/contents/ui/Config.qml:39
#, kde-format
msgctxt "@item:inlistbox"
msgid "User Processes"
msgstr "Procesos de usuarios"

#: src/faces/processtable/contents/ui/Config.qml:40
#, kde-format
msgctxt "@item:inlistbox"
msgid "System Processes"
msgstr "Procesos do sistema"

#: src/faces/processtable/contents/ui/Config.qml:41
#, kde-format
msgctxt "@item:inlistbox"
msgid "All Processes"
msgstr "Todos os procesos"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:28
#, kde-format
msgctxt "@action"
msgid "Search"
msgstr "Buscar"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:39
#, kde-format
msgctxt "@action"
msgid "End Process"
msgstr "Finalizar o proceso"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:53
#, kde-format
msgctxt "@action"
msgid "Display as List"
msgstr "Amosar como unha lista"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:70
#, kde-format
msgctxt "@action"
msgid "Display as Tree"
msgstr "Amosar como unha árbore"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:82
#, kde-format
msgctxt "@action %1 is view type"
msgid "Show: %1"
msgstr "Amosar: %1"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:85
#, kde-format
msgctxt "@item:inmenu"
msgid "Own Processes"
msgstr "Procesos propios"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:94
#, kde-format
msgctxt "@item:inmenu"
msgid "User Processes"
msgstr "Procesos de usuarios"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:103
#, kde-format
msgctxt "@item:inmenu"
msgid "System Processes"
msgstr "Procesos do sistema"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:112
#, kde-format
msgctxt "@item:inmenu"
msgid "All Processes"
msgstr "Todos os procesos"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:124
#, kde-format
msgctxt "@action"
msgid "Configure columns…"
msgstr "Configurar as columnas…"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:172
#, kde-format
msgctxt "@action:inmenu"
msgid "Send Signal"
msgstr "Enviar o sinal"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:175
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Suspend (STOP)"
msgstr "Suspender (STOP)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:179
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Continue (CONT)"
msgstr "Continuar (CONT)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:183
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Hangup (HUP)"
msgstr "Cortar (HUP)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:187
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Interrupt (INT)"
msgstr "Interromper (INT)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:191
#, kde-format
msgctxt "@action:inmenu  Send Signal"
msgid "Terminate (TERM)"
msgstr "Terminar (TERM)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:195
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "Kill (KILL)"
msgstr "Matar (KILL)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:199
#, kde-format
msgctxt "@action:inmenu Send Signal"
msgid "User 1 (USR1)"
msgstr "Usuario 1 (USR1)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:203
#, kde-format
msgctxt "@action:inmenu  Send Signal"
msgid "User 2 (USR2)"
msgstr "Usuario 2 (USR2)"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:211
#, kde-format
msgctxt "@action:inmenu"
msgid "End Process"
msgid_plural "End %1 Processes"
msgstr[0] "Finalizar o proceso"
msgstr[1] "Finalizar %1 procesos"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:223
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Columns…"
msgstr "Configurar as columnas…"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:233
#, kde-format
msgctxt "@title:window"
msgid "End Process"
msgid_plural "End %1 Processes"
msgstr[0] "Finalizar o proceso"
msgstr[1] "Finalizar %1 procesos"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:234
#, kde-format
msgctxt "@action:button"
msgid "End Process"
msgid_plural "End Processes"
msgstr[0] "Finalizar o proceso"
msgstr[1] "Finalizar os procesos"

#: src/faces/processtable/contents/ui/FullRepresentation.qml:236
#, kde-format
msgid ""
"Are you sure you want to end this process?\n"
"Any unsaved work may be lost."
msgid_plural ""
"Are you sure you want to end these %1 processes?\n"
"Any unsaved work may be lost."
msgstr[0] ""
"Seguro que quere finalizar o proceso?\n"
"Pode perder o traballo que non gardase."
msgstr[1] ""
"Seguro que quere finalizar os %1 procesos?\n"
"Pode perder o traballo que non gardase."

#: src/faces/processtable/contents/ui/FullRepresentation.qml:243
#, kde-format
msgctxt "@item:intable %1 is process id, %2 is user name"
msgid "Process ID %1, owned by %2"
msgstr "Identificador de proceso %1, propiedade de %2"
